package org.training.oauth.authorization.server.configuration;

import java.util.UUID;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		
		httpSecurity
			.authorizeHttpRequests(authorizeRequests -> 
					authorizeRequests.anyRequest().authenticated())
					.formLogin(Customizer.withDefaults());
		
		return httpSecurity.build();
	}

    @Bean
    RegisteredClientRepository registeredClientRepository() {
	
	  RegisteredClient registeredClient = RegisteredClient.withId(UUID.randomUUID().toString())
	    .clientId("huongdanjava")
	    .clientSecret("{noop}123456")
	    .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
	    .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
	    .redirectUri("https://oidcdebugger.com/debug")
	    .scope("test")
	    .build();
	  
	 
	  return new InMemoryRegisteredClientRepository(registeredClient);
	}
	
    @Bean
    UserDetailsService users() {
      UserDetails user = User.withUsername("admin")
          .password("{noop}password")
          .roles("admin")
          .build();

      return new InMemoryUserDetailsManager(user);
    }
	
	
}
